package main.action;

import com.opensymphony.xwork2.ActionSupport;
import main.model.MessageStore;

/**
 * Created with IntelliJ IDEA.
 * User: bernardoneill
 * Date: 10/11/2012
 * Time: 12:26
 * To change this template use File | Settings | File Templates.
 */
public class HelloWorldAction extends ActionSupport {

    private static final long serialVersionUID = 1L;
    private MessageStore messageStore;

    public String execute() throws Exception {
        messageStore = new MessageStore();
        return SUCCESS;
    }

    public MessageStore getMessageStore() {
        return messageStore;
    }

    public void setMessageStore(MessageStore messageStore) {
        this.messageStore = messageStore;
    }
}
