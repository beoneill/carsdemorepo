package main.action;

import com.opensymphony.xwork2.ActionSupport;
import main.controller.CarManager;
import main.model.Car;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Car: bernardoneill
 * Date: 11/11/2012
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
public class CarAction extends ActionSupport {
    private static final long serialVersionUID = 50L;
    private Car car;
    private List<Car> carList;
    private List<String> colours;
    private Long id;

    private CarManager carManager;

    public CarAction() {
        carManager = new CarManager();
        colours = new ArrayList<String>();
        colours.add("Black");
        colours.add("White");
        colours.add("Red");
        colours.add("Navy");
        colours.add("Silver");
    }

    public String execute() {
        System.out.println("execute called");
        this.carList = carManager.list();
        return SUCCESS;
    }

    public String add() {
        System.out.println(getCar());
        try {
            carManager.add(getCar());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.carList = carManager.list();
        return SUCCESS;
    }

    public String delete() {
        carManager.delete(getId());
        this.carList = carManager.list();
        return SUCCESS;
    }

    public Car getCar() {
        return car;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public List<String> getColours() {
        return colours;
    }

    public void setColours(List<String> searchEngine) {
        this.colours = colours;
    }

    public String getDefaultColour() {
        return "Black";
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
