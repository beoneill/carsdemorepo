package main.action;

import com.opensymphony.xwork2.ActionSupport;
import main.controller.UserManager;
import main.model.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: bernardoneill
 * Date: 11/11/2012
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
public class UserAction extends ActionSupport {

    private static final long serialVersionUID = 50L;
    private User user;
    private List<User> userList;
    private Long id;

    private UserManager userManager;

    public UserAction() {
        userManager = new UserManager();
    }

    public String execute() {
        this.userList = userManager.list();
        System.out.println("execute called");
        return SUCCESS;
    }

//    public String validateUser() {
//
//        for(User user : userManager.list()) {
//            if (user.getFirstName().equals(username) && user.getPassword().equals(pass)) {
//                return SUCCESS;
//            } else  {
//                return ERROR;
//            }
//        }
//
//        return ERROR;
//
//    }

    public String add() {
        System.out.println(getUser());
        try {
            userManager.add(getUser());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.userList = userManager.list();
        return SUCCESS;
    }

    public String delete() {
        userManager.delete(getId());
        this.userList = userManager.list();
        return SUCCESS;
    }

    public User getUser() {
        return user;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserList(List<User> usersList) {
        this.userList = usersList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
