package main.controller;

import main.model.Car;
import main.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Car: bernardoneill
 * Date: 11/11/2012
 * Time: 18:16
 * To change this template use File | Settings | File Templates.
 */
public class CarManager extends HibernateUtil {
    public Car add(Car car) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(car);
        session.getTransaction().commit();
        return car;
    }
    public Car delete(Long id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Car car = (Car) session.load(Car.class, id);
        if(null != car) {
            session.delete(car);
        }
        session.getTransaction().commit();
        return car;
    }

    public List<Car> list() {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Car> cars = null;
        try {

            cars = (List<Car>)session.createQuery("from Car").list();

        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        }
        session.getTransaction().commit();
        return cars;
    }
}

