package main.controller;

import main.model.User;
import main.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: bernardoneill
 * Date: 11/11/2012
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */
public class UserManager extends HibernateUtil {
    public User add(User user) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        return user;
    }
    public User delete(Long id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        User user = (User) session.load(User.class, id);
        if(null != user) {
            session.delete(user);
        }
        session.getTransaction().commit();
        return user;
    }

    public List<User> list() {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<User> users = null;
        try {

            users = (List<User>)session.createQuery("from User").list();

        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        }
        session.getTransaction().commit();
        return users;
    }
}
