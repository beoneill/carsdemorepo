package main.model;

/**
 * Created with IntelliJ IDEA.
 * User: bernardoneill
 * Date: 11/11/2012
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import main.controller.UserManager;
import org.omg.PortableInterceptor.SUCCESSFUL;

import java.util.Map;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.*;

public class LoginResult extends ActionSupport {
    private String errormessage;
    private String username;
    private String password;

    public String execute(){
        Map session = ActionContext.getContext().getSession();  // Get session
        UserManager userManager = new UserManager();

        for(User user : userManager.list()) {
            if (user.getFirstName().equals(getUsername()) && user.getPassword().equals(getPassword())) {
                session.put("loggedin","true");
                session.put("userId",user.getId());
                return "SUCCESS";
            }
        }
    setErrormessage("Unable to login, please try again.");
    return "ERROR";
    }

    /* getters and setters */
    public String getErrormessage() {
        return errormessage;
    }
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
}
