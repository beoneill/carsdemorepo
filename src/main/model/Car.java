package main.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: bernardoneill
 * Date: 11/11/2012
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name="CAR")
public class Car implements Serializable {

    private static final long serialVersionUID = 2L;

    private Long id;
    private String userId;
    private String make;
    private String model;
    private String colour;

    @Id
    @GeneratedValue
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="userId")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name="make")
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Column(name="model")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    @Column(name="colour")
    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

}
