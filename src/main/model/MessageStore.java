package main.model;

/**
 * Created with IntelliJ IDEA.
 * User: bernardoneill
 * Date: 10/11/2012
 * Time: 12:11
 * To change this template use File | Settings | File Templates.
 */
public class MessageStore {

    private String message;

    public MessageStore() {
        setMessage("Hello World");
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
