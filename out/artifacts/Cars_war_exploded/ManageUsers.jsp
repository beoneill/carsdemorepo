<%--
  Created by IntelliJ IDEA.
  User: bernardoneill
  Date: 10/11/2012
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
    <title>User Manager - Struts2 Hibernate Example</title>
</head>
<body>

<h1>User Manager</h1>
<s:actionerror/>

<s:form action="add" method="post">
    <s:textfield name="user.firstName" label="Firstname"/>
    <s:textfield name="user.lastName" label="Lastname"/>
    <s:textfield name="user.password" label="Password"/>
    <s:submit value="Add User" align="center"/>
</s:form>


<h2>Users</h2>
<table>
    <tr>
        <th>Name</th>
    </tr>
    <s:iterator value="userList" var="user">
        <tr>
            <td><s:property value="lastName"/>, <s:property value="firstName"/> </td>
            <td><a href="delete?id=<s:property value="id"/>">delete</a></td>
        </tr>
    </s:iterator>
</table>
</body>
</html>