<%--
  Created by IntelliJ IDEA.
  User: bernardoneill
  Date: 10/11/2012
  Time: 12:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
    <title>Enter a Car</title>
    <link rel="stylesheet" href="stylesheet.css" type="text/css"/>
    <script type="text/javascript" src="//use.typekit.net/jyd2vbg.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>
<body>
<div class="outer">
    <div class="inner">
       <h1>Enter your car details</h1>
       <s:form action="addCar" method="post" cssClass="loginForm">
           <s:textfield name="car.make" label="Make" cssClass="textInput"/>
           <s:textfield name="car.model" label="Model" cssClass="textInput"/>
           <s:select label="colour" key="car.colour" list="colours" value="defaultColour"/>
           <s:hidden name="car.userId" value="%{#session.userId}"/>
           <s:submit value="Add Car" align="center" cssClass="submitInput"/>
       </s:form>
       <table class="list">
           <s:iterator value="carList" var="car">
               <s:if test="%{#session.userId == #car.userId}">
                   <tr>
                       <td><s:property value="make"/></td>
                       <td><s:property value="model"/></td>
                       <td><s:property value="colour"/></td>
                       <td class="delete"><a href="deleteCar?id=<s:property value="id"/>">delete</a></td>
                   </tr>
               </s:if>
           </s:iterator>
       </table>
        </div>
    </div>
</body>
</html>