<%--
  Created by IntelliJ IDEA.
  User: bernardoneill
  Date: 10/11/2012
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
    <link rel="stylesheet" href="stylesheet.css" type="text/css"/>
    <script type="text/javascript" src="//use.typekit.net/jyd2vbg.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <title>Log in</title>
</head>
<body>
    <div class="outer">
        <div class="inner">
            <h1>Welcome to Car Chooser</h1>
            <s:actionerror/>

            <s:form action="CheckLogin" method="POST" cssClass="loginForm">
                <s:textfield name="username" label="Firstname"/>
                <s:password name="password" label="Password"/>
                <s:submit value="Login" align="center"/>
            </s:form>

            <s:div id="errormessage"><s:property value="errormessage" /></s:div>
        </div>
    </div>
</body>
</html>